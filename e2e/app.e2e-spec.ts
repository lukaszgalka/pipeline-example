import { PipelineExamplePage } from './app.po';

describe('pipeline-example App', () => {
  let page: PipelineExamplePage;

  beforeEach(() => {
    page = new PipelineExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
